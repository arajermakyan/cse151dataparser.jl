using CSE151DataParser
using Base.Test

# write your own tests here
@test 1 == 1
tests = ["unit_tests","high_level_tests"]
for t in tests
  include(joinpath(Pkg.dir("CSE151DataParser"),"test",t,"runtests.jl"))
end
