using Base.Test

println("Running High Level Tests")
println("...")

# write your own tests here
@test 1 == 1
tests = ["statistics"]
for t in tests
  include("$(t).jl")
end
println("...")

println("Finished High Level Tests")
