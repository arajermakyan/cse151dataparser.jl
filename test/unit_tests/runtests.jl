using Base.Test

# write your own tests here
println("Running Unit Tests")
println("...")
@test 1 == 1
tests = ["data_reader","threshold_sampling","read_data_into_memory"]
for t in tests
  include("$(t).jl")
end

println("...")
println("Finished Unit Tests")
