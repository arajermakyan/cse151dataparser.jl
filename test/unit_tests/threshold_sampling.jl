using Base.Test

println("Running threshold_sampling tests")
path = joinpath(Pkg.dir("CSE151DataParser"),"resources","abalone.data")
threshold = .1
count = CSE151DataParser.getDataSetCount(path)
srand(123)
trainingset = CSE151DataParser.getTrainingSetIndices(path,count,threshold)
srand(123)
testset = CSE151DataParser.getTestSetIndices(path,count,threshold)

@test length(intersect(trainingset,testset))==0
@test length(union(trainingset,testset))==4177
@test length(trainingset)==round(count*(1-threshold))
@test length(testset)==round(count*(threshold))
@test in(0,trainingset)==false
@test in(0,testset)==false
#CSE151Seeder.closeDataSet(datastream)
