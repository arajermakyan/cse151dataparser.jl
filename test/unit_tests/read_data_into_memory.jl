using Base.Test


println("Running read_data_into_memory tests")

threshold = .1
seed=123
resourcepath = joinpath(Pkg.dir("CSE151DataParser"),"resources")



function testMemory(path,threshold,seed,datatype)

  count = CSE151DataParser.getDataSetCount(path)

  srand(seed)

  trainingset = CSE151DataParser.getTrainingSet(path,threshold,datatype)

  srand(seed)
  testset = CSE151DataParser.getTestSet(path,threshold,datatype)

  featureCount= CSE151DataParser.getFeatureCount(path)


  @test size(testset)==(round(count*threshold),featureCount)

  @test size(trainingset)==(round(count*(1-threshold)),featureCount)

end
testMemory(joinpath(resourcepath,"abalone.data"),threshold,seed,Any)
testMemory(joinpath(resourcepath,"3percent-miscategorization.csv"),threshold,seed,Float64)
testMemory(joinpath(resourcepath,"10percent-miscatergorization.csv"),threshold,seed,Float64)
testMemory(joinpath(resourcepath,"Seperable.csv"),threshold,seed,Float64)
