using Base.Test


println("Running data_reader tests")

path = joinpath(Pkg.dir("CSE151DataParser"),"resources","abalone.data")
#datastream = CSE151Seeder.getDataSet(path)

@test CSE151DataParser.getDataSetCount(path) ==4177
@test CSE151DataParser.getFeatureCount(path) ==9
#chceks reset stream
#@test CSE151Seeder.getDataSetCount(datastream) ==4177

#CSE151Seeder.closeDataSet(datastream)
#@test_throws SystemError CSE151Seeder.getDataSetCount(datastream)
