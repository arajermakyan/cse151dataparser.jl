# CSE151DataParser

#### This is the Data Parser for CSE151.

## Installation
Open up julia and do:

```julia
Pkg.clone("git@bitbucket.org:arajermakyan/cse151dataparser.jl.git")
Pkg.resolve()
```

To use the module:
```julia
using CSE151DataParser
```
##Test
To run the tests, run
```julia
Pkg.test("CSE151DataParser")
```

##Uninstall
To remove this package run:
```julia
Pkg.rm("CSE151DataParser")
```
