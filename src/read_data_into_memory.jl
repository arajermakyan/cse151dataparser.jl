function getDataSetInMemory(file,threshold,datatype,datasetFunction)
  size = getDataSetCount(file)
  indices = datasetFunction(file,size,threshold)
  featureCount = getFeatureCount(file);
  stream = getDataSet(file)
  dataset = cell(length(indices),featureCount)
  index=0;
  loc=1

  for line in eachline(stream)
    index+=1
    if in(index,indices)
      dataset[loc,:]= split(strip(line,'\n'),",")
      loc+=1
    end

  end
  #println(size(testset))

  closeDataSet(stream)

  return dataset
end


function getTestSet(file,threshold,datatype=Any)
  return getDataSetInMemory(file,threshold,datatype,getTestSetIndices)


end


function getTrainingSet(file,threshold,datatype=Any)
  return getDataSetInMemory(file,threshold,datatype,getTrainingSetIndices)
end
