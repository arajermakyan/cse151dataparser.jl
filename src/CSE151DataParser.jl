module CSE151DataParser
#export getTrainingSet,getTestSet,getDataSet

  include("data_reader.jl")
  include("threshold_sampling.jl")
  include("read_data_into_memory.jl")
# package code goes here

end # module
