#assume that srand() will be called! this is important
function getTrainingSetIndices(file,size,threshold)
  number_remaining =size
  number_needed =convert(Int,round(number_remaining*(1-threshold)))
#  println(number_needed)
  trainingset= zeros(Int,number_needed,1)
  probability = number_needed/number_remaining
  line=0;
  index =0;
  dataset = getDataSet(file)
  for i in enumerate(eachline(dataset))
    #println(i)
    if number_needed==0
      break;
    end

    line+=1
    number_remaining-=1
    if rand() > (1-probability)
      index+=1
      trainingset[index]=line
      number_needed-=1
    end

    probability = number_needed/number_remaining


  end
  closeDataSet(dataset)
  return trainingset
end

function getTestSetIndices(file,size, threshold)
  number_remaining =size;
  number_needed =convert(Int,round(number_remaining*(threshold)))
#  println(number_needed)
  testset= zeros(Int,number_needed,1)
  probability = number_needed/number_remaining
  line=0;
  index =0;
  dataset = getDataSet(file)
  for i in enumerate(eachline(dataset))
    #println(i)
    if number_needed==0
      break;
    end

    line+=1
    number_remaining-=1
    if rand() <= (probability)
      index+=1
      testset[index]=line
      number_needed-=1
    end

    probability = number_needed/number_remaining


  end
  closeDataSet(dataset)
  return testset
end
